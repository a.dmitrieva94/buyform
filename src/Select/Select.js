import React from 'react';
import Select, { components } from 'react-select';
import { allCountries } from './helper';
import './Select.scss';

const DropdownIndicator = props => {
  return (
    <components.DropdownIndicator {...props}>
      <div className={`indicator ${props.isFocused ? 'open' : ''}`}/>
    </components.DropdownIndicator>
  );
};

const IndicatorSeparator = () => null;

export const CustomSelect = ({hasError, value, ...props}) => {
  return (
    <Select
      className={`selectWrapper ${hasError ? 'borderRed' : ''}`}
      classNamePrefix='coutry'
      components={{ DropdownIndicator, IndicatorSeparator }}
      value={value === '' ? null : {label: value, value}}
      options={allCountries}
      isSearchable
      {...props}
      theme={theme => ({
        ...theme,
        colors: {
          ...theme.colors,
          primary: '#101D94',
          boxShadow: 'none'
        },
      })}
    />
  );
}
