import React from 'react';
import InputMask from 'react-input-mask';
import './Input.scss';

export const Input = ({title, mask, hasError=false, ...props}) => {
  return <div className='inputWrapper'>
    {!!title && <h5>{title}</h5>}
    {mask
      ? <InputMask
        {...props}
        mask={mask}
        className={hasError ? 'borderRed' : ''}
      />
      : <input
        {...props}
        className={hasError ? 'borderRed' : ''}
      />
    }
  </div>
};
