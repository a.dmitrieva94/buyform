import React from 'react';
import done from '../image/done.png';
import './SuccessfulScreen.scss';

export const SuccessfulScreen = () => {
  return <div className='SuccessfulScreen'>
    <img src={done} alt='done'/>
    <p>Спасибо!</p>
  </div>
}
