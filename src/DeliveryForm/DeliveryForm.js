import React, { useRef, useContext, useState, useCallback } from 'react';
import { Input } from '../Input/Input';
import { CustomSelect } from '../Select/Select';
import { AppContext } from '../App';
import { validateIndex } from '../validators';
import './DeliveryForm.scss';

export const DeliveryForm = () => {
  const [hasError, setHasError] = useState({
    recipient: false,
    city: false,
    address: false,
    nation: false,
    index: false,
  });
  const {state: { recipient, city, address, nation, index }, changeState} = useContext(AppContext);
  const formRef = useRef(null);

  const onChangeField = useCallback((e, option) => {
    if(option && option.name === 'nation'){
      changeState(option.name, e.value);
      setHasError({
        ...hasError,
        [option.name]: false
      });
    } else {
      changeState(e.target.name, e.target.value);
      setHasError({
        ...hasError,
        [e.target.name]: false
      });
    }
  // eslint-disable-next-line
  }, [hasError]);

  const submitForm = (e) => {
    e.preventDefault();
    if(recipient && city && address && nation && validateIndex(index)){
      changeState('stepNumber', 2);
    } else {
      setHasError({
        recipient: recipient === '',
        city: city === '',
        address: address === '',
        nation: nation === '',
        index: !validateIndex(index),
      });
    }
  };

  return <div className='formWrapper'>
    <h3>Информация для доставки</h3>
    <form className='deliveryForm' ref={formRef} onSubmit={submitForm}>
      <div className='recipient'>
        <Input
          title='Получатель'
          tabIndex={1}
          placeholder='ФИО'
          name='recipient'
          hasError={hasError.recipient}
          value={recipient}
          onChange={onChangeField}
        />
      </div>
      <Input
        title='Адрес'
        tabIndex={2}
        placeholder='Город'
        name='city'
        hasError={hasError.city}
        value={city}
        onChange={onChangeField}
      />
      <Input
        tabIndex={3}
        placeholder='Адрес'
        name='address'
        hasError={hasError.address}
        value={address}
        onChange={onChangeField}
      />
      <div className="lastRow">
        <CustomSelect
          tabIndex={4}
          placeholder='Страна'
          name='nation'
          hasError={hasError.nation}
          value={nation}
          onChange={onChangeField}
        />
        <Input
          tabIndex={5}
          placeholder='Индекс'
          name='index'
          hasError={hasError.index}
          mask={'999 999'}
          value={index}
          onChange={onChangeField}
        />
      </div>
      <button  tabIndex={6} type='submit'>Продолжить</button>
    </form>
  </div>
};
