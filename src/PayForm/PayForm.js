import React, { useRef, useContext, useState, useCallback } from 'react';
import { Input } from '../Input/Input';
import { AppContext } from '../App';
import { validateCardCvv, validateCardDate, validateCardNumber } from '../validators';
import './PayForm.scss';

export const PayForm = () => {
  const [hasError, setHasError] = useState({
    card_name: false,
    card_number: false,
    card_date: false,
    card_cvv: false
  });
  const {state: {card_name, card_number, card_date, card_cvv}, changeState} = useContext(AppContext);
  const formPayRef = useRef(null);

  const onChangeField = useCallback((e) => {
    debugger;
    setHasError({
      ...hasError,
      [e.target.name]: false
    });
    changeState(e.target.name, e.target.value);
  // eslint-disable-next-line
  }, [hasError]);

  const submitForm = (e) => {
    e.preventDefault();
    if(card_name && validateCardNumber(card_number) && validateCardDate(card_date) && validateCardCvv(card_cvv)){
      changeState("stepNumber", 3);
    } else {
      setHasError({
        card_name: card_name === '',
        card_number: !validateCardNumber(card_number),
        card_date: !validateCardDate(card_date),
        card_cvv: !validateCardCvv(card_cvv)
      });
    }
  };

  return <div className='formWrapper'>
    <h3>Оплата</h3>
    <form className='deliveryForm  payForm' ref={formPayRef} onSubmit={submitForm}>
      <div className='recipient card_name'>
        <Input
          title='Имя на карте'
          tabIndex={1}
          placeholder='Konstantin Ivanov'
          name='card_name'
          hasError={hasError.card_name}
          value={card_name}
          onChange={onChangeField}
        />
      </div>
      <Input
        title='Номер карты'
        tabIndex={2}
        placeholder='XXXX XXXX XXXX XXXX XXXX'
        mask={'9999-9999-9999-9999'}
        name='card_number'
        hasError={hasError.card_number}
        value={card_number}
        onChange={onChangeField}
      />
      <div className="lastRow">
        <Input
          tabIndex={3}
          title='Срок'
          placeholder='MM / YY'
          name='card_date'
          hasError={hasError.card_date}
          mask={'99/99'}
          value={card_date}
          onChange={onChangeField}
        />
        <Input
          tabIndex={3}
          title='CVV'
          autoComplete='off'
          placeholder=''
          name='card_cvv'
          hasError={hasError.card_cvv}
          mask={'999'}
          value={card_cvv}
          onChange={onChangeField}
        />
      </div>
      <button tabIndex={5} type='submit'>Оплатить</button>
    </form>
  </div>
};
