import React, { Component } from 'react';
import { SuccessfulScreen } from './SuccessfulScreen/SuccessfulScreen';
import { DeliveryForm } from './DeliveryForm/DeliveryForm';
import './App.scss';
import { PayForm } from './PayForm/PayForm';
import { Crumbs } from './Crumbs/Crumbs';
export const AppContext = React.createContext();

class App extends Component {
  state = {
    recipient: '',
    city: '',
    address: '',
    nation: '',
    index: '',
    card_name: '',
    card_number: '',
    card_date: '',
    card_cvv: '',
    stepNumber: 1,
  };

  changeState = (fieldName, fieldValue) => {
    this.setState({[fieldName]: fieldValue });
  };

  render(){
    const { stepNumber } = this.state;

    return (
      <AppContext.Provider
        value={{ state: this.state, changeState: this.changeState }}
      >
        <div className="App">
          {stepNumber !== 3
            && <Crumbs />
          }
          {stepNumber === 1
            && <DeliveryForm />
          }
          {stepNumber === 2
            &&  <PayForm />
          }
          {stepNumber === 3
            && <SuccessfulScreen />
          }
        </div>
      </AppContext.Provider>
    );
  }
}

export default App;
