import React, { useContext } from 'react';
import { AppContext } from '../App';
import { validateIndex } from '../validators';
import './Crumbs.scss';

export const Crumbs = () => {
  const {
    state: {recipient, city, address, nation, index, stepNumber},
    changeState
  } = useContext(AppContext);

  const clickByCrumb = (e) => {
    changeState("stepNumber", Number(e.target.dataset.num));
  };

  const  allowNextStep = recipient !== ''
    && city !== ''
    && address !== ''
    && nation !== ''
    && validateIndex(index);

  return  <ul className='crumbs'>
    <li className={stepNumber === 1 ? 'active' : ''} data-num='1' onClick={clickByCrumb}>Доставка</li>
    <li
      className={`${stepNumber === 2 ? 'active' : ''} ${allowNextStep ? '' : 'disabled'}`}
      data-num='2'
      onClick={allowNextStep ? clickByCrumb : undefined}
    >
      Оплата
    </li>
  </ul>
};
