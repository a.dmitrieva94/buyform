export const validateIndex = (value) => {
  return value.replace(/[^0-9]/g, '').length === 6;
};

export const validateCardNumber = (value) => {
  return value.replace(/[^0-9]/g, '').length === 16;
};

export const validateCardDate = (value) => {
  const valueArr = value.split('/');
  const now = new Date();
  if (value === ''){
    return false
  } else if (
    Number(valueArr[1]) > now.getFullYear() - 2000
    || (Number(valueArr[1]) === now.getFullYear() - 2000 && Number(valueArr[0]) >= now.getMonth() + 1)
  ){
    return true
  }
  return false;
};

export const validateCardCvv = (value) => {
  return value.replace(/[^0-9]/g, '').length === 3;
};
